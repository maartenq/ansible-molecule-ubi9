# Ansible Molecule UBI9

[![Docker Repository on Quay](https://quay.io/repository/maartenq/ansible-molecule-ubi9/status "Docker Repository on Quay")](https://quay.io/repository/maartenq/ansible-molecule-ubi9)

Dockerfile for building container image for use for Ansible Molecule testing on UBI9. Supplies systemd init and *ansible* sudo user.
